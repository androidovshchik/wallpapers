<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(400);
    exit;
}

if (!isset($_POST['package']) || !is_string($_POST['package'])) {
    http_response_code(400);
    exit;
}

if (!preg_match("/^[a-z0-9_]+\.[a-z0-9_]+\.[a-z0-9_]+$/", $_POST['package'])) {
    http_response_code(400);
    exit;
}

if (!is_dir(__DIR__ . '/../images/' . $_POST['package'])) {
    http_response_code(400);
    exit;
}

require_once __DIR__ . '/../vendor/autoload.php';

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

$db = new Medoo\Medoo(json_decode(file_get_contents(__DIR__ . '/mysql_db.json'), true));

$data = $db->select("pictures", ["path", "view"], [
    "package" => $_POST['package']
]);

$files = [];

try {
    $adapter = new Local(__DIR__ . '/../images/' . $_POST['package'], LOCK_SH);
    $filesystem = new Filesystem($adapter);
    $filesystem->addPlugin(new League\Flysystem\Plugin\ListFiles());
    foreach ($filesystem->listFiles('', true) as $file) {
        array_push($files, [
            'path' => $file['path'],
            'time' => $file['timestamp']
        ]);
    }
} catch (Exception $e) {
}

header('Content-Type: application/json; charset=utf-8');

echo json_encode([
    "db" => $data,
    "fs" => $files
], JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
