<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(400);
    exit;
}

if (!isset($_POST['package']) || !is_string($_POST['package']) || !isset($_POST['path']) || !is_string($_POST['path'])) {
    http_response_code(400);
    exit;
}

if (!preg_match("/^[a-z0-9_]+\.[a-z0-9_]+\.[a-z0-9_]+$/", $_POST['package'])) {
    http_response_code(400);
    exit;
}

if (!is_dir(__DIR__ . '/../images/' . $_POST['package'])) {
    http_response_code(400);
    exit;
}

$filePath = str_replace("/../", "//", '/' . $_POST['path']);
$filePath = str_replace("/./", "//", $filePath);

if (strpos($filePath, "//") !== false || !is_file(__DIR__ . '/../images/' . $_POST['package'] . $filePath)) {
    http_response_code(400);
    exit;
}

require_once __DIR__ . '/../vendor/autoload.php';

$db = new Medoo\Medoo(json_decode(file_get_contents(__DIR__ . '/mysql_db.json'), true));

$result = $db->update("pictures", [
    "view[+]" => 1
], [
    "AND" => [
        "package" => $_POST['package'],
        "path" => $_POST['path']
    ]
]);
if ($result->rowCount() <= 0) {
    $result = $db->insert("pictures", [
        "package" => $_POST['package'],
        "path" => $_POST['path']
    ]);
}

header('Content-Type: application/json; charset=utf-8');

echo json_encode([
    "result" => $result->rowCount()
], JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
