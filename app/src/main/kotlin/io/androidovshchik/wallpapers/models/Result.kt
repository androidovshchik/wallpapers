package io.androidovshchik.wallpapers.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Result : Serializable {

    @SerializedName("result")
    var result = 0

    override fun toString(): String {
        return "Result(result=$result)"
    }
}