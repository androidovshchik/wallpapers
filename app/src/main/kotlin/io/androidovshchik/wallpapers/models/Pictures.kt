package io.androidovshchik.wallpapers.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Pictures : Serializable {

    @SerializedName("db")
    var db = arrayListOf<Picture>()

    @SerializedName("fs")
    var fs = arrayListOf<Picture>()

    override fun toString(): String {
        return "Pictures(db=$db, fs=$fs)"
    }
}