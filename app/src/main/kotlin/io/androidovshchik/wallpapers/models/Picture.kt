package io.androidovshchik.wallpapers.models

import android.content.Context
import androidx.room.*
import com.google.gson.annotations.SerializedName
import io.androidovshchik.wallpapers.BuildConfig
import java.io.Serializable

@Entity(tableName = "pictures", indices = [
    Index(value = ["path"], unique = true)
])
class Picture : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null

    @SerializedName("path")
    @ColumnInfo(name = "path")
    var path = ""

    @Ignore
    @SerializedName("time")
    var time = -1

    @SerializedName("view")
    @ColumnInfo(name = "v")
    var view = 0

    @ColumnInfo(name = "favorite")
    var favorite = false

    fun getImageUrl(context: Context): String {
        return if (BuildConfig.REMOTE) {
            "http://greenqaq2.beget.tech/wallpapers/images/${context.packageName}/$path"
        } else "file:///android_asset/wallpapers/images/$path"
    }

    fun getThumbUrl(context: Context): String {
        return if (BuildConfig.REMOTE) {
            "http://greenqaq2.beget.tech/wallpapers/thumbs/${context.packageName}/$path"
        } else "file:///android_asset/wallpapers/thumbs/$path"
    }

    override fun toString(): String {
        return "Picture(id=$id, path='$path', time=$time, view=$view, favorite=$favorite)"
    }
}