package io.androidovshchik.wallpapers.data

import android.database.sqlite.SQLiteConstraintException
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.androidovshchik.wallpapers.models.Picture

@Dao
interface PictureDao {

    @Query("SELECT * FROM pictures")
    fun getAll(): List<Picture>

    @Query("SELECT * FROM pictures WHERE favorite = 1")
    fun getFavorites(): List<Picture>

    @Query("SELECT * FROM pictures WHERE path = :path LIMIT 1")
    fun getPicture(path: String): Picture?

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertPicture(picture: Picture)

    @Query("UPDATE pictures SET v = :view WHERE path = :path")
    fun updateView(path: String, view: Int)

    @Query("UPDATE pictures SET favorite = :favorite WHERE path = :path")
    fun updateFavorite(path: String, favorite: Int)
}

fun PictureDao.upsertView(picture: Picture) {
    try {
        insertPicture(picture)
    } catch (e: SQLiteConstraintException) {
        updateView(picture.path, picture.view)
    }
}

fun PictureDao.upsertFavorite(picture: Picture) {
    try {
        insertPicture(picture)
    } catch (e: SQLiteConstraintException) {
        updateFavorite(picture.path, if (picture.favorite) 1 else 0)
    }
}