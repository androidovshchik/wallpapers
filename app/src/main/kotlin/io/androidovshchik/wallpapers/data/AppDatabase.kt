package io.androidovshchik.wallpapers.data

import androidx.room.Database
import androidx.room.RoomDatabase
import io.androidovshchik.wallpapers.models.Picture

@Database(entities = [Picture::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pictureDao(): PictureDao
}