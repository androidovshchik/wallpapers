@file:Suppress("unused")

package io.androidovshchik.wallpapers.data

import io.androidovshchik.wallpapers.models.Pictures
import io.androidovshchik.wallpapers.models.Result
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface ServerApi {

    @GET("images/{package}/{path}")
    fun originalImage(@Path("package") pckg: String, @Path("path") path: String): Observable<ResponseBody>

    /**
     * @param view only needed for local flavour and equals to current view count (without plus 1)
     */
    @FormUrlEncoded
    @POST("api/add-view.php")
    fun addView(@Field("package") pckg: String, @Field("path") path: String, @Field("view") view: Int): Observable<Result>

    @FormUrlEncoded
    @POST("api/get-all.php")
    fun getAll(@Field("package") pckg: String): Observable<Pictures>
}