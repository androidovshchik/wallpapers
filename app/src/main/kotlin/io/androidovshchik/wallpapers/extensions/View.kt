/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused")

package io.androidovshchik.wallpapers.extensions

import android.content.Context
import android.view.View
import com.google.android.material.snackbar.Snackbar

private typealias Click = (View) -> Unit

fun View.appContext(): Context = context.applicationContext

fun View.isVisible(): Boolean = this.visibility == View.VISIBLE

fun View.isInvisible(): Boolean = this.visibility == View.INVISIBLE

fun View.isGone(): Boolean = this.visibility == View.GONE

fun View.setVisible() {
    this.visibility = View.VISIBLE
}

fun View.setInvisible() {
    this.visibility = View.INVISIBLE
}

fun View.setGone() {
    this.visibility = View.GONE
}

fun View.snackbar(message: CharSequence) = Snackbar
    .make(this, message, Snackbar.LENGTH_SHORT)
    .apply { show() }

fun View.snackbar(message: CharSequence, actionText: CharSequence, action: Click) = Snackbar
    .make(this, message, Snackbar.LENGTH_INDEFINITE)
    .setAction(actionText, action)
    .apply { show() }
