@file:Suppress("unused")

package io.androidovshchik.wallpapers.extensions

import android.content.Context
import android.os.Environment
import com.snatik.storage.Storage
import io.androidovshchik.wallpapers.R
import java.io.File
import java.util.*

fun Storage.getAppPath(context: Context): String {
    return "${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)}$sep${context.getString(R.string.app_name)}"
}

fun Storage.appendRandom(path: String): String {
    val lastDot = path.lastIndexOf(".")
    val random = Random()
    return when {
        lastDot > 0 -> path.substring(0, lastDot) + "-" + random.nextString(8..8) + path.substring(lastDot, path.length)
        lastDot == 0 -> random.nextString(8..8) + path
        else -> path + random.nextString(8..8)
    }
}

fun Storage.getValidName(path: String): String = "${getValidNameWithoutExt(path)}.${getFile(path).extension}"

fun Storage.getValidNameWithoutExt(path: String): String = getFile(path).nameWithoutExtension
    .replace("[^A-Za-z0-9-_]+".toRegex(), "")

fun Storage.getParentPath(file: File): String = file.parentFile.path

fun Storage.getParentPath(file: String): String = getParentPath(getFile(file))