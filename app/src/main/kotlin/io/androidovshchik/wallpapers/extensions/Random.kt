/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused")

package io.androidovshchik.wallpapers.extensions

import java.util.*

const val NUMBERS = "0123456789"
const val UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const val LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz"
const val LETTERS = UPPER_LETTERS + LOWER_LETTERS
const val CHARS = NUMBERS + LETTERS

fun Random.nextInt(range: IntRange) = range.start + nextInt(range.last - range.start + 1)

fun Random.nextString(range: IntRange, chars: String = CHARS): String {
    var value = ""
    for (i in 0 until nextInt(range)) {
        value += chars[Math.floor(Math.random() * chars.length).toInt()]
    }
    return value
}