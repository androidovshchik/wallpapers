/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers

import android.annotation.SuppressLint
import io.androidovshchik.wallpapers.base.BaseApplication
import timber.log.Timber

@SuppressLint("Registered")
@Suppress("unused")
abstract class ReleaseApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(ReleaseTree())
    }
}