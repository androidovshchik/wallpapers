/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.gui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.extensions.context.startActionView
import io.androidovshchik.wallpapers.extensions.setCompoundXmlDrawables
import kotlinx.android.synthetic.main.activity_gms.*

class GMSActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gms)
        setTitle(R.string.title_gms)
        update.setCompoundXmlDrawables(R.drawable.ic_shop_white_24dp)
        update.setOnClickListener {
            if (!startActionView("market://details?id=" + GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE)) {
                startActionView("https://play.google.com/store/apps/details?id=" +
                    GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(applicationContext) == ConnectionResult.SUCCESS) {
            startActivity(packageManager.getLaunchIntentForPackage(packageName)!!.apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            })
            finish()
        }
    }

    override fun onBackPressed() {}
}