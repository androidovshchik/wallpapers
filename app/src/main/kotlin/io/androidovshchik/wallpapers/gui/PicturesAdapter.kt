/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.gui

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import io.androidovshchik.wallpapers.GlideApp
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.base.BaseAdapter
import io.androidovshchik.wallpapers.base.BaseViewHolder
import io.androidovshchik.wallpapers.extensions.inflate
import io.androidovshchik.wallpapers.models.Picture
import kotlinx.android.synthetic.main.item_picture.view.*

@Suppress("unused")
class PicturesAdapter : BaseAdapter<Picture>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_picture), viewType)
    }

    override fun onViewRecycled(holder: BaseViewHolder<Picture>) {
        super.onViewRecycled(holder)
        GlideApp.with((holder as ViewHolder).appContext)
            .clear(holder.thumb)
    }

    inner class ViewHolder(itemView: View, viewType: Int) : BaseViewHolder<Picture>(itemView, viewType) {

        val thumb: ImageView = itemView.thumb

        init {
            itemView.setOnClickListener {
                listener?.invoke(adapterPosition, items[adapterPosition])
            }
        }

        override fun onBindItem(position: Int, item: Picture) {
            GlideApp.with(appContext)
                .load(item.getThumbUrl(appContext))
                .into(thumb)
        }
    }
}