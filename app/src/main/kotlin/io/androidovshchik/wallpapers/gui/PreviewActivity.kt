@file:Suppress("DEPRECATION")

package io.androidovshchik.wallpapers.gui

import android.content.ClipData
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.annotation.UiThread
import androidx.annotation.WorkerThread
import androidx.core.content.FileProvider
import androidx.room.Room
import com.snatik.storage.Storage
import io.androidovshchik.wallpapers.BuildConfig
import io.androidovshchik.wallpapers.GlideApp
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.base.BaseV7Activity
import io.androidovshchik.wallpapers.data.AppDatabase
import io.androidovshchik.wallpapers.data.upsertFavorite
import io.androidovshchik.wallpapers.extensions.*
import io.androidovshchik.wallpapers.extensions.context.scanFile
import io.androidovshchik.wallpapers.extensions.context.toastShort
import io.androidovshchik.wallpapers.models.Picture
import io.androidovshchik.wallpapers.models.Result
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_preview.*
import timber.log.Timber
import java.io.IOException

class PreviewActivity : BaseV7Activity() {

    private val contentView: View by lazy {
        findViewById<View>(android.R.id.content)
    }

    private val dialog by lazy {
        newProgressDialog().apply {
            setCancelable(false)
        }
    }

    private lateinit var picture: Picture

    private lateinit var db: AppDatabase

    private lateinit var storage: Storage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)
        storage = Storage(applicationContext)
        picture = Picture().apply {
            path = intent.getStringExtra("path")
        }
        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "app.db").build()
        disposable.add(Observable.fromCallable {
            db.pictureDao().getPicture(picture.path)?.let {
                picture.id = it.id
                picture.view = it.view
                picture.favorite = it.favorite
            }
            true
        }.switchMap {
            if (BuildConfig.REMOTE) {
                return@switchMap Observable.just(Result())
            } else {
                return@switchMap baseApp.serverApi.addView(packageName, picture.path, picture.view)
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                toggleFavorite()
            }, {
                Timber.e(it)
                toastShort("$it")
            }))
        if (BuildConfig.REMOTE) {
            baseApp.serverApi.addView(packageName, picture.path, 0)
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
        GlideApp.with(applicationContext)
            .asBitmap()
            .load(picture.getImageUrl(applicationContext))
            .into(preview)
        install.setOnClickListener {
            picture.favorite = true
            toggleFavorite()
            setWallpapers(true, false)
        }
        download.setOnClickListener {
            dialog.show()
            disposable.add(baseApp.serverApi.originalImage(packageName, picture.path)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    val dir = storage.getAppPath(applicationContext)
                    storage.createDirectory(dir)
                    val path = dir + sep + storage.appendRandom(storage.getValidName(picture.path))
                    try {
                        storage.createFile(path, it.bytes())
                        if (!isFinishing) {
                            runOnUiThread {
                                scanFile(path)
                                dialog.dismiss()
                                showInterstitial(R.string.ads_interstitial3, false)
                                contentView.snackbar(getString(R.string.saved_image, path),
                                    getString(android.R.string.ok)) {}
                            }
                        }
                    } catch (e: IOException) {
                        showError(e)
                    }
                }, {
                    showError(it)
                }))
        }
        like.setOnClickListener {
            picture.favorite = !picture.favorite
            toggleFavorite()
        }
        toggleFavorite()
        if (intent.getIntExtra("countAds2", 0) % resources.getInteger(R.integer.count_interstitial2) == 0) {
            showInterstitial(R.string.ads_interstitial2, false)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun setWallpapers(system: Boolean, lock: Boolean) {
        dialog.show()
        disposable.add(baseApp.serverApi.originalImage(packageName, picture.path)
            .subscribeOn(Schedulers.io())
            .subscribe({
                try {
                    val ext = storage.getFile(picture.path).extension.toLowerCase()
                    val path = "${getExternalFilesDir(Environment.DIRECTORY_PICTURES)}/wallpaper.$ext"
                    val file = storage.getFile(path)
                    storage.createDirectory(storage.getParentPath(file))
                    storage.createFile(path, it.bytes())
                    if (!isFinishing) {
                        runOnUiThread {
                            dialog.dismiss()
                            startWallIntent(FileProvider.getUriForFile(applicationContext, "$packageName.remote", file))
                        }
                    }
                } catch (e: IOException) {
                    showError(e)
                }
            }, {
                showError(it)
            }))
    }

    @UiThread
    private fun startWallIntent(uri: Uri) {
        val intent = Intent(Intent.ACTION_ATTACH_DATA).apply {
            type = "image/*"
            data = uri
            putExtra("mimeType", "image/*")
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                clipData = ClipData.newRawUri("", uri)
            }
            addCategory(Intent.CATEGORY_DEFAULT)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            val activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            for (activity in activities) {
                val packageName = activity.activityInfo.packageName
                grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.app_name)), 200)
    }

    @WorkerThread
    private fun showError(e: Throwable) {
        Timber.e(e)
        if (!isFinishing) {
            runOnUiThread {
                dialog.dismiss()
                toastShort("$e")
            }
        }
    }

    private fun toggleFavorite() {
        like.setImageResource(if (picture.favorite) {
            R.drawable.ic_favorite_24dp
        } else R.drawable.ic_favorite_border_white_24dp)
    }

    override fun onStop() {
        disposable.add(Observable.fromCallable {
            db.pictureDao().upsertFavorite(picture)
            true
        }.subscribeOn(Schedulers.io())
            .subscribe())
        super.onStop()
    }

    override fun onBackPressed() {
        disposable.add(Observable.fromCallable {
            db.pictureDao().upsertFavorite(picture)
            true
        }.subscribeOn(Schedulers.io())
            .subscribe({
                setResult(RESULT_OK)
                finish()
            }, {
                Timber.e(it)
                setResult(RESULT_OK)
                finish()
            }))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 200) {
            dialog.dismiss()
            showInterstitial(R.string.ads_interstitial4, false)
        }
    }

    override fun onDestroy() {
        dialog.dismiss()
        GlideApp.with(applicationContext)
            .clear(preview)
        super.onDestroy()
    }
}
