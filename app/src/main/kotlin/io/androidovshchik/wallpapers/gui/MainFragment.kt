package io.androidovshchik.wallpapers.gui

import android.os.Bundle
import android.view.View
import androidx.room.Room
import io.androidovshchik.wallpapers.BuildConfig
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.base.BaseV4Fragment
import io.androidovshchik.wallpapers.data.AppDatabase
import io.androidovshchik.wallpapers.extensions.context.toastShort
import io.androidovshchik.wallpapers.models.Pictures
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main.*
import timber.log.Timber

class MainFragment : BaseV4Fragment() {

    override val layout = R.layout.fragment_main

    var position = -1

    private val adapter = PicturesAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter.setAdapterListener { _, item ->
            (activity as MainActivity?)?.openPreview(item)
        }
        recycler_view.addItemDecoration(PicturesItemDecoration())
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = adapter
    }

    fun onPictures(pictures: Pictures) {
        when (position) {
            0 -> {
                pictures.db.forEach { dbPicture ->
                    pictures.fs.forEach { fsPicture ->
                        if (dbPicture.path == fsPicture.path) {
                            fsPicture.view = dbPicture.view
                        }
                    }
                }
                adapter.items.clear()
                adapter.items.addAll(pictures.fs.sortedWith(compareByDescending { it.view }))
                adapter.notifyDataSetChanged()
            }
            1 -> {
                adapter.items.clear()
                if (BuildConfig.REMOTE) {
                    adapter.items.addAll(pictures.fs.sortedWith(compareByDescending { it.time }))
                } else {
                    adapter.items.addAll(pictures.fs)
                }
                adapter.notifyDataSetChanged()
            }
            2 -> {
                adapter.items.clear()
                if (BuildConfig.REMOTE) {
                    disposable.add(Observable.fromCallable {
                        val db: AppDatabase = Room.databaseBuilder(context
                            ?: return@fromCallable false,
                            AppDatabase::class.java, "app.db").build()
                        db.pictureDao().getFavorites().forEach { dbPicture ->
                            pictures.fs.forEach { fsPicture ->
                                if (dbPicture.path == fsPicture.path) {
                                    adapter.items.add(dbPicture)
                                }
                            }
                        }
                        true
                    }.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            adapter.notifyDataSetChanged()
                        }, {
                            Timber.e(it)
                            context?.toastShort("$it")
                        }))
                } else {
                    pictures.db.forEach { dbPicture ->
                        pictures.fs.forEach { fsPicture ->
                            if (dbPicture.favorite && dbPicture.path == fsPicture.path) {
                                adapter.items.add(dbPicture)
                            }
                        }
                    }
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }
}