/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.gui

import android.Manifest
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.viewpager.widget.ViewPager
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.base.BasePagerAdapter
import io.androidovshchik.wallpapers.base.BaseV7Activity
import io.androidovshchik.wallpapers.extensions.context.toastShort
import io.androidovshchik.wallpapers.extensions.requestPermissions
import io.androidovshchik.wallpapers.models.Picture
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : BaseV7Activity() {

    private lateinit var adapter: BasePagerAdapter

    private var shownAds1 = false

    private var countAds2 = 0

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val contentView = findViewById<View>(android.R.id.content)
            contentView.setPadding(0, getStatusBarHeight(), 0, 0)
            contentView.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.navigationBarColor = Color.parseColor("#4c000000")
        }
        title = getString(R.string.main_title)
        setSupportActionBar(toolbar)
        adapter = BasePagerAdapter(supportFragmentManager).apply {
            fragments.add(MainFragment().also {
                it.title = getString(R.string.tab_popular)
                it.position = 0
            })
            fragments.add(MainFragment().also {
                it.title = getString(R.string.tab_new)
                it.position = 1
            })
            fragments.add(MainFragment().also {
                it.title = getString(R.string.tab_favorite)
                it.position = 2
            })
        }
        viewpager.offscreenPageLimit = 3
        viewpager.adapter = adapter
        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                if (!shownAds1) {
                    shownAds1 = true
                    showInterstitial(R.string.ads_interstitial1, false)
                }
            }
        })
        tabs.setupWithViewPager(viewpager)
        swipe_layout.setOnRefreshListener {
            loadPictures()
        }
        loadPictures()
    }

    private fun loadPictures() {
        disposable.clear()
        disposable.add(baseApp.serverApi.getAll(packageName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                swipe_layout.isRefreshing = false
                adapter.fragments.forEach { fragment ->
                    (fragment as MainFragment).onPictures(it)
                }
            }, {
                Timber.e(it)
                toastShort("$it")
            }))
    }

    fun openPreview(picture: Picture) {
        disposable.add(requestPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe({ result ->
                if (result.isGranted) {
                    countAds2++
                    startActivityForResult(Intent(applicationContext, PreviewActivity::class.java).apply {
                        putExtra("path", picture.path)
                        putExtra("countAds2", countAds2)
                    }, 100)
                }
            }, {
                Timber.e(it)
                toastShort("$it")
            }))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_menu -> {
                val bottomSheetFragment = MainSheetFragment()
                bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 100 && resultCode == RESULT_OK) {
            loadPictures()
        }
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }
}
