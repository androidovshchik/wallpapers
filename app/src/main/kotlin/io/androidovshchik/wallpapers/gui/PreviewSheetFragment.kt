package io.androidovshchik.wallpapers.gui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.androidovshchik.wallpapers.R
import kotlinx.android.synthetic.main.preview_sheet.*

class PreviewSheetFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.preview_sheet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        variant1.setOnClickListener {
            (activity as PreviewActivity?)?.setWallpapers(true, false)
            dismiss()
        }
        variant2.setOnClickListener {
            (activity as PreviewActivity?)?.setWallpapers(false, true)
            dismiss()
        }
        variant3.setOnClickListener {
            (activity as PreviewActivity?)?.setWallpapers(true, true)
            dismiss()
        }
    }
}