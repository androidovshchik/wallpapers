package io.androidovshchik.wallpapers.gui

import android.content.res.Resources
import android.graphics.Rect
import android.view.View

import androidx.recyclerview.widget.RecyclerView

class PicturesItemDecoration : RecyclerView.ItemDecoration() {

    private val space = (4 * Resources.getSystem().displayMetrics.density).toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.right = space
        outRect.bottom = space
        if (parent.getChildLayoutPosition(view) < 2) {
            outRect.top = space
        } else {
            outRect.top = 0
        }
        if (parent.getChildLayoutPosition(view) % 2 == 0) {
            outRect.left = space
        } else {
            outRect.left = 0
        }
    }
}