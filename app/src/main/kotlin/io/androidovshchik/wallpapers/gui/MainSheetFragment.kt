package io.androidovshchik.wallpapers.gui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.base.BaseApplication
import io.androidovshchik.wallpapers.extensions.context.openMarket
import kotlinx.android.synthetic.main.main_sheet.*

class MainSheetFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.main_sheet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        review.setOnClickListener {
            context?.openMarket()
            dismiss()
        }
        support.setOnClickListener {
            BaseApplication.sendEmail(context ?: return@setOnClickListener, "", "")
            dismiss()
        }
    }
}