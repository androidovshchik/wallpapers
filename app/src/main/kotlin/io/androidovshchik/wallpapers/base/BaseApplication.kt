/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import com.google.android.gms.ads.MobileAds
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import io.androidovshchik.wallpapers.BuildConfig
import io.androidovshchik.wallpapers.R
import io.androidovshchik.wallpapers.data.ServerApi
import io.androidovshchik.wallpapers.extensions.context.toastShort
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

@SuppressLint("Registered")
@Suppress("unused")
abstract class BaseApplication : MultiDexApplication() {

    abstract val serverApi: ServerApi

    override fun onCreate() {
        super.onCreate()
        if (isMainProcess()) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
            // Создание расширенной конфигурации библиотеки.
            val config = YandexMetricaConfig.newConfigBuilder(getString(R.string.yandex_key)).build()
            // Инициализация AppMetrica SDK.
            YandexMetrica.activate(applicationContext, config)
            // Отслеживание активности пользователей.
            YandexMetrica.enableActivityAutoTracking(this)
            if (!BuildConfig.DEBUG) {
                MobileAds.initialize(applicationContext, getString(R.string.ads_app))
            }
            ViewPump.init(ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                    CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Montserrat-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()))
                .build())
        }
    }

    open fun isMainProcess(): Boolean {
        return true
    }

    protected fun buildServerApi(): Retrofit = Retrofit.Builder()
        .client(OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor { message ->
                    Timber.tag("NETWORK")
                        .d(message)
                }.apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
        }.connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build())
        .baseUrl("http://greenqaq2.beget.tech/wallpapers/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    companion object {

        fun sendEmail(context: Context, subject: String, text: String) {
            try {
                context.startActivity(Intent.createChooser(Intent(Intent.ACTION_SEND).apply {
                    type = "message/rfc822"
                    putExtra(Intent.EXTRA_EMAIL, arrayOf(context.getString(R.string.support_email)))
                    putExtra(Intent.EXTRA_SUBJECT, subject)
                    putExtra(Intent.EXTRA_TEXT, text)
                }, context.getString(R.string.send_via)))
            } catch (e: Exception) {
                Timber.e(e)
                context.toastShort("$e")
            }
        }
    }
}