/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.base

import androidx.recyclerview.widget.RecyclerView

private typealias Listener<T> = (position: Int, item: T) -> Unit

@Suppress("unused")
abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseViewHolder<T>>() {

    @Suppress("MemberVisibilityCanBePrivate")
    val items = arrayListOf<T>()

    protected var listener: Listener<T>? = null

    fun setAdapterListener(listener: Listener<T>?) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int) = position

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.onBindItem(position, items[position])
    }

    override fun getItemCount() = items.size
}