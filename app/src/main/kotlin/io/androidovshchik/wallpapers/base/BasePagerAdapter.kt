/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.base

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

@Suppress("MemberVisibilityCanBePrivate", "unused")
class BasePagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {

    val fragments: ArrayList<BaseV4Fragment> = arrayListOf()

    override fun getItem(position: Int): BaseV4Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int) = fragments[position].title

    override fun getItemPosition(any: Any) = POSITION_NONE
}