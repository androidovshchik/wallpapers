/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import io.androidovshchik.wallpapers.extensions.context.newIntent
import io.androidovshchik.wallpapers.gui.GMSActivity
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

@SuppressLint("Registered")
@Suppress("MemberVisibilityCanBePrivate")
open class BaseV7Activity : AppCompatActivity() {

    protected val disposable = CompositeDisposable()

    val baseApp: BaseApplication
        get() = application as BaseApplication

    private var isActive = false

    private var isLoadingAds = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(applicationContext) != ConnectionResult.SUCCESS) {
            startActivity(newIntent(GMSActivity::class.java))
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        isActive = true
    }

    override fun onStop() {
        isActive = false
        super.onStop()
    }

    protected fun showInterstitial(id: Int, finish: Boolean) {
        if (isLoadingAds) {
            return
        }
        isLoadingAds = true
        val interstitialAd = InterstitialAd(this)
        interstitialAd.adUnitId = getString(id)
        interstitialAd.adListener = object : AdListener() {

            override fun onAdLoaded() {
                Timber.d("onAdLoaded")
                if (!isFinishing && isActive) {
                    interstitialAd.show()
                } else {
                    isLoadingAds = false
                }
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                Timber.e("onAdFailedToLoad errorCode=$errorCode")
                if (!isFinishing && finish) {
                    finish()
                }
                isLoadingAds = false
            }

            override fun onAdOpened() {
                Timber.d("onAdOpened")
            }

            override fun onAdLeftApplication() {
                Timber.d("onAdLeftApplication")
            }

            override fun onAdClosed() {
                Timber.d("onAdClosed")
                if (!isFinishing && finish) {
                    finish()
                }
                isLoadingAds = false
            }
        }
        interstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}