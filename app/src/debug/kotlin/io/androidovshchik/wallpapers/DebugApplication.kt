/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers

import android.annotation.SuppressLint
import com.facebook.stetho.Stetho
import io.androidovshchik.wallpapers.base.BaseApplication
import org.acra.ACRA
import org.acra.ReportField
import org.acra.ReportingInteractionMode
import org.acra.config.ConfigurationBuilder
import timber.log.Timber

@SuppressLint("Registered")
@Suppress("unused")
abstract class DebugApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
        if (!isMainProcess()) {
            return
        }
        Stetho.initializeWithDefaults(applicationContext)
        ACRA.init(this, ConfigurationBuilder(this)
            .setMailTo(getString(R.string.developer_email))
            .setReportingInteractionMode(ReportingInteractionMode.DIALOG)
            .setResDialogTheme(R.style.DialogTheme_Support)
            .setResDialogText(R.string.error_crash)
            .setResDialogCommentPrompt(R.string.error_comment)
            .setCustomReportContent(
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.BRAND,
                ReportField.PHONE_MODEL,
                ReportField.PRODUCT,
                ReportField.USER_COMMENT,
                ReportField.USER_APP_START_DATE,
                ReportField.USER_CRASH_DATE,
                ReportField.STACK_TRACE,
                ReportField.LOGCAT
            ))
    }

    override fun isMainProcess(): Boolean {
        return !ACRA.isACRASenderServiceProcess()
    }
}