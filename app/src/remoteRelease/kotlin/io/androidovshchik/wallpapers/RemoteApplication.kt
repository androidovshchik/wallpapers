/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers

import android.annotation.SuppressLint
import io.androidovshchik.wallpapers.data.ServerApi

@SuppressLint("Registered")
@Suppress("unused")
class RemoteApplication : ReleaseApplication() {

    override val serverApi: ServerApi = buildServerApi()
        .create(ServerApi::class.java)
}