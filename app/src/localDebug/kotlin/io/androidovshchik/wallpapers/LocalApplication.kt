/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.wallpapers

import android.annotation.SuppressLint

@SuppressLint("Registered")
@Suppress("unused")
class LocalApplication : DebugApplication() {

    override val serverApi = ServerApiMock()

    override fun onCreate() {
        super.onCreate()
        if (isMainProcess()) {
            serverApi.setContext(applicationContext)
        }
    }
}