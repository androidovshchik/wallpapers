package io.androidovshchik.wallpapers

import android.content.ContentProvider
import android.content.ContentValues
import android.content.res.AssetFileDescriptor
import android.database.Cursor
import android.net.Uri
import android.os.ParcelFileDescriptor
import timber.log.Timber
import java.io.FileNotFoundException
import java.io.IOException

class LocalProvider : ContentProvider() {

    override fun onCreate(): Boolean {
        Timber.d("onCreate")
        return true
    }

    override fun openAssetFile(uri: Uri, mode: String): AssetFileDescriptor? {
        Timber.d("openAssetFile: $uri $mode")
        return try {
            context?.assets?.openFd(uri.toString().replace("content://${context?.packageName}.local/",
                "wallpapers/images/"))
        } catch (e: IOException) {
            Timber.e(e)
            null
        }
    }

    @Throws(FileNotFoundException::class)
    override fun openFile(uri: Uri, mode: String): ParcelFileDescriptor? {
        Timber.d("openFile: $uri $mode")
        return super.openFile(uri, mode)
    }

    override fun getType(p1: Uri): String? {
        Timber.d("getType")
        return "image/*"
    }

    override fun delete(p1: Uri, p2: String?, p3: Array<String>?): Int {
        Timber.d("delete")
        return 0
    }

    override fun query(p1: Uri, p2: Array<String>?, p3: String?, p4: Array<String>?, p5: String?): Cursor? {
        Timber.d("query")
        return null
    }

    override fun insert(p1: Uri, p2: ContentValues?): Uri? {
        Timber.d("insert")
        return null
    }

    override fun update(p1: Uri, p2: ContentValues?, p3: String?, p4: Array<String>?): Int {
        Timber.d("update")
        return 0
    }
}