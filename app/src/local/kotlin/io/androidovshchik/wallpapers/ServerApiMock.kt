package io.androidovshchik.wallpapers

import android.content.Context
import androidx.room.Room
import io.androidovshchik.wallpapers.data.AppDatabase
import io.androidovshchik.wallpapers.data.ServerApi
import io.androidovshchik.wallpapers.data.upsertView
import io.androidovshchik.wallpapers.models.Picture
import io.androidovshchik.wallpapers.models.Pictures
import io.androidovshchik.wallpapers.models.Result
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.ResponseBody
import timber.log.Timber
import java.io.IOException
import java.util.concurrent.TimeUnit

class ServerApiMock : ServerApi {

    private var context: Context? = null

    private var db: AppDatabase? = null

    fun setContext(context: Context) {
        this.context = context
        db = Room.databaseBuilder(context, AppDatabase::class.java, "app.db").build()
    }

    override fun originalImage(pckg: String, path: String): Observable<ResponseBody> {
        return Observable.fromCallable {
            context?.assets?.open("wallpapers/images/$path")?.use {
                ResponseBody.create(MediaType.get("image/*"), it.readBytes())
            } ?: throw NullPointerException()
        }.delay(200, TimeUnit.MILLISECONDS)
    }

    override fun addView(pckg: String, path: String, view: Int): Observable<Result> {
        return Observable.fromCallable {
            db?.pictureDao()?.upsertView(Picture().apply {
                this.path = path
                this.view = view + 1
            })
            Result().apply {
                result = 1
            }
        }.delay(200, TimeUnit.MILLISECONDS)
    }

    override fun getAll(pckg: String): Observable<Pictures> {
        return Observable.fromCallable {
            val response = Pictures()
            response.db.addAll(db?.pictureDao()?.getAll() ?: arrayListOf())
            try {
                listAssetFiles("wallpapers/images").forEach {
                    response.fs.add(Picture().apply {
                        path = it
                        time = 0
                    })
                }
            } catch (e: IOException) {
                Timber.e(e)
            }
            response
        }.delay(200, TimeUnit.MILLISECONDS)
    }

    private fun listAssetFiles(path: String): List<String> {
        val result = arrayListOf<String>()
        context?.assets?.list(path)?.forEach {
            val innerFiles = listAssetFiles("$path/$it")
            if (innerFiles.isNotEmpty()) {
                result.addAll(innerFiles)
            } else {
                result.add("$path/$it".substring("wallpapers/images/".length))
            }
        }
        return result
    }
}